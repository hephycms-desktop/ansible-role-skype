Skype
=========

Install Skype for Linux

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.skype

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
